> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Katie Ragen

### Assignment #2 Includes:

* Screenshot of Activity 1 running on Nexus 5 API 23 in Android Studio
* Screenshot of Activity 2 running on Nexus 5 API 23 in Android Studio
* Link to BitBucket repo for this assignment
* Healthy Recipes Bruschetta app

#### Assignment Screenshots:

*Screenshot of Activity 1 running*:

![AMPPS Installation Screenshot](img/activity1.png)

*Screenshot of Activity 2 running*:

![JDK Installation Screenshot](img/activity2.png)

#### Links:

*Bitbucket repo for this assignment*
[A1 Bitbucket Repo Link](https://bitbucket.org/katieragen/lis4381/a2 "A1 Bitbucket Repo")