> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Katie Ragen

### Assignment #1 Requirements:

*Includes:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch. 1 & 2)
4. Bitbucket repo links: (a) this assignment and (b) completed tutorials (bitbucketstationlocations and myteamquotes)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Git commands w/short descriptions:

1. git init - creates a new local repository
2. git status - lists the files you've changed and those you still need to add or commit
3. git add - adds one or more files to staging, in preparation to commit
4. git commit - commits any files you've added and any files changed since then
5. git push - sends changes to the master branch of remote reposoitory
6. git pull - fetches and merges changes on remote server to working directory
7. git remote -v - lists all currently configured remote repositories

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

#### Links:

*Bitbucket Repo For This Project*
[A1 Bitbucket Repo Link](https://bitbucket.org/katieragen/lis4381/ "A1 Bitbucket Repo")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/katieragen/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/katieragen/myteamquotes/ "My Team Quotes Tutorial")
